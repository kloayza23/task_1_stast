import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from scipy import stats
data=np.genfromtxt('data.dat')
#Calcule las siguientes Estadísticas Descriptivas : Media, moda, varianza, desviación, mínimo y máximo
print("---------------**Compute of Descriptive Statistical")
print("Mean:"+str(np.mean(data)))
print("Mode:"+str(stats.mode(data)[0]))
print("Variance:"+str(np.var(data)))
print("Standard Deviation:"+str(np.std(data)))
#Construya una Tabla de Frecuencias
print("---------------**Frecuency Table**---------------")
unique, counts = np.unique(data, return_counts=True)
print (np.asarray((unique, counts)).T)
#A partir de la tabla anterior, construya el Histograma
print("---------------**Histogram**---------------")
hist, bin_edges = np.histogram(data, density=True)
plt.bar(bin_edges[:-1], hist, width = 1)
plt.xlim(min(bin_edges), max(bin_edges))
#plt.show()  
#Construya el Diagrama de Cajas. ¿Existen valores aberrantes?
print("---------------**Boxplot**---------------")
fig = plt.figure(1, figsize=(9, 6))
ax = fig.add_subplot(111)
bp = ax.boxplot(data)
fig.savefig('fig1.png', bbox_inches='tight')
print("In the graph, there are outliers.")